//
//  PSInfiniteScrollViewTests.m
//  PSInfiniteScrollViewTests
//
//  Created by Felipe B. Valio on 3/5/13.
//  Copyright (c) 2013 Pinuts Studios. All rights reserved.
//

#import "PSInfiniteScrollViewTests.h"

@implementation PSInfiniteScrollViewTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in PSInfiniteScrollViewTests");
}

@end
