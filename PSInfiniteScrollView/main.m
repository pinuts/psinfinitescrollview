//
//  main.m
//  PSInfiniteScrollView
//
//  Created by Felipe B. Valio on 3/5/13.
//  Copyright (c) 2013 Pinuts Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PSAppDelegate class]));
    }
}
