//
//  PSAppDelegate.h
//  PSInfiniteScrollView
//
//  Created by Felipe B. Valio on 3/5/13.
//  Copyright (c) 2013 Pinuts Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSViewController;

@interface PSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) PSViewController *viewController;

@end
