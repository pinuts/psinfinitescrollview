//
//  PSLateralScroller.h
//  iBrittle
//
//  Created by Felipe on 12/9/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class PSInfiniteScrollView;

@protocol PSInfiniteScrollViewDelegate <UIScrollViewDelegate>
- (UIView*) infiniteScrollView: (PSInfiniteScrollView*) scrollView viewAtIndex: (int) index;
- (int) numberOfViewsForInfiniteScrollView: (PSInfiniteScrollView*) scrollView;

@optional;
- (void) didSelectViewOfInfiniteScrollView: (PSInfiniteScrollView*) scrollView;
- (void) infiniteScrollView: (PSInfiniteScrollView*) lateralScroller didChangePage: (int) page;
@end

@interface PSInfiniteScrollView : UIScrollView {
    UIGestureRecognizer *recognizer;
}

@property (nonatomic, weak) id<PSInfiniteScrollViewDelegate> delegate;
@property (nonatomic, readonly) int index;

- (void) reloadData;

@end
