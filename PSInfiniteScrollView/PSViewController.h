//
//  PSViewController.h
//  PSInfiniteScrollView
//
//  Created by Felipe B. Valio on 3/5/13.
//  Copyright (c) 2013 Pinuts Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSInfiniteScrollView.h"

@interface PSViewController : UIViewController <PSInfiniteScrollViewDelegate>

@end
