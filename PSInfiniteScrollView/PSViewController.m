//
//  PSViewController.m
//  PSInfiniteScrollView
//
//  Created by Felipe B. Valio on 3/5/13.
//  Copyright (c) 2013 Pinuts Studios. All rights reserved.
//

#import "PSViewController.h"

@interface PSViewController ()
@property (strong, nonatomic) IBOutlet PSInfiniteScrollView *scrollView;
@end

@implementation PSViewController

- (void)viewDidUnload {
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (IBAction)didChangeSegmentValue:(UISegmentedControl*)sender {
    self.scrollView.pagingEnabled = (sender.selectedSegmentIndex == 0);
}


#pragma mark - PSInfiniteScrollView Delegate Methods

- (int) numberOfViewsForInfiniteScrollView: (PSInfiniteScrollView*) scrollView {
    return 5;
}

- (UIView*) infiniteScrollView: (PSInfiniteScrollView*) scrollView viewAtIndex: (int) index {
    
    UILabel *label = [UILabel new];
    
    label.font = [UIFont systemFontOfSize:50];
    label.text = [NSString stringWithFormat:@"%i", index+1];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor redColor];
    label.backgroundColor = [UIColor colorWithWhite:rand()%256/255.f alpha:1];
    
    return label;
}


- (void) didSelectViewOfInfiniteScrollView: (PSInfiniteScrollView*) scrollView {
    
    NSString *msg = [NSString stringWithFormat:@"Selected page %i", scrollView.index+1];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@""
                                                   message:msg
                                                  delegate:nil
                                         cancelButtonTitle:@"Ok"
                                         otherButtonTitles:nil];
    [alert show];
}


- (void) infiniteScrollView: (PSInfiniteScrollView*) lateralScroller didChangePage: (int) page {
    NSLog(@"changed page to %i", page+1);
}


@end
