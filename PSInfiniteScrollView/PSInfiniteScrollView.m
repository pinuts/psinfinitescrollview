//
//  PSLateralScroller.m
//  iBrittle
//
//  Created by Felipe on 12/9/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "PSInfiniteScrollView.h"

@interface PSInfiniteScrollView()
@property (nonatomic, strong) UIView *page1;
@property (nonatomic, strong) UIView *page2;
@property (nonatomic, strong) UIView *page3;
@end

@implementation PSInfiniteScrollView

@synthesize delegate;
@synthesize index;
@synthesize page1;
@synthesize page2;
@synthesize page3;

//__________________________________________________________________________________________________________________________________

- (void) scrollToCenter {
    super.contentOffset = CGPointMake(self.frame.size.width, 0);
}

//__________________________________________________________________________________________________________________________________

- (int) indexIncremented {
    if( delegate == nil ) {
        return 0;
    }
    
    int n = [delegate numberOfViewsForInfiniteScrollView:self];
    
    return (index + 1) % n;
}

//__________________________________________________________________________________________________________________________________

- (int) indexDecremented {
    if( delegate == nil )
        return 0;
    
    int num = [delegate numberOfViewsForInfiniteScrollView:self];
    return (index - 1 + num) % num;
}

//__________________________________________________________________________________________________________________________________

- (void) setFrameOfView: (UIView*) view forPage: (int) page {
    CGRect frame = view.frame;

    frame.origin.x = page * self.frame.size.width;
    frame.size = self.frame.size;
    view.frame = frame;
}

//__________________________________________________________________________________________________________________________________

- (void) didSelectView: (UIGestureRecognizer*) gesture {
    if( [delegate respondsToSelector:@selector(didSelectViewOfInfiniteScrollView:)] ) {
        [delegate didSelectViewOfInfiniteScrollView:self];
    }
}

- (void)setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated {
    NSLog(@"content: %@", NSStringFromCGPoint(contentOffset));
    [super setContentOffset:contentOffset animated:animated];
}

//__________________________________________________________________________________________________________________________________

- (void) myInit {
    self.showsHorizontalScrollIndicator = NO;
    self.alwaysBounceVertical = NO;
    
    recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectView:)];
    [self addGestureRecognizer:recognizer];
    
    index = 0;
    
    if( [delegate numberOfViewsForInfiniteScrollView:self] == 0 ) {
        return;
    }
    
    page1 = [delegate infiniteScrollView:self viewAtIndex:[self indexDecremented]];
    page2 = [delegate infiniteScrollView:self viewAtIndex:index];
    page3 = [delegate infiniteScrollView:self viewAtIndex:[self indexIncremented]];
    
    [self updatePages];

    [self addSubview:page1];
    [self addSubview:page2];
    [self addSubview:page3];
}

//__________________________________________________________________________________________________________________________________

- (void) setDelegate:(id<PSInfiniteScrollViewDelegate>)aDelegate {
    delegate = aDelegate;
    super.delegate = delegate;
    [self myInit];
}

//__________________________________________________________________________________________________________________________________

- (void) reloadData {
    [self myInit];
}

//__________________________________________________________________________________________________________________________________

- (void)updatePages {
    
    self.contentSize = CGSizeMake(3*self.frame.size.width, self.frame.size.height);

    [self setFrameOfView:page1 forPage:0];
    [self setFrameOfView:page2 forPage:1];
    [self setFrameOfView:page3 forPage:2];
    
    [self scrollToCenter];
}

//__________________________________________________________________________________________________________________________________

- (void) setFrame:(CGRect)frame {
    super.frame = frame;
    [self updatePages];
}

//__________________________________________________________________________________________________________________________________

- (void) layoutSubviews {
    [super layoutSubviews];
    
    BOOL flag = NO;
    
	if( self.contentOffset.x >= 2*self.frame.size.width ) {
        
        index = [self indexIncremented];
        
        [page1 removeFromSuperview];
        
        page1 = page2;
        page2 = page3;
        page3 = [delegate infiniteScrollView:self viewAtIndex:[self indexIncremented]];
        
        [self addSubview:page3];
        
        self.contentOffset = CGPointMake(self.contentOffset.x-self.frame.size.width,
                                         self.contentOffset.y);
        flag = YES;
	}
    
	else if( self.contentOffset.x <= 0 ) {
        
        index = [self indexDecremented];
        
        [page3 removeFromSuperview];
        
        page3 = page2;
        page2 = page1;
        page1 = [delegate infiniteScrollView:self viewAtIndex:[self indexDecremented]];
        
        [self addSubview:page1];
        
        self.contentOffset = CGPointMake(self.contentOffset.x+self.frame.size.width,
                                         self.contentOffset.y);
        flag = YES;
	}
    
    if( flag ) {
        [self updatePages];
        if( [delegate respondsToSelector:@selector(infiniteScrollView:didChangePage:)] ) {
            [delegate infiniteScrollView:self didChangePage:index];
        }
    }
}


@end
